# Projet IOT : Compteur entrée/sortie de salle.

## Fetch from Source

```shell
git clone https://gitlab.etude.eisti.fr/laubachers/projet_iot.git
```

## Arduino :

### Pins arduino uilisés :

- Capteurs extérieurs :
  - pins triggers : *lTrigPin*=3 ; *rTrigPin*=9
  - pins echos : *outLeft*=2 ; *outRight*=8
- Capteurs intérieurs :
  - pin trigger : *mTrigger*=9
  - pin echo : *middle*=4
- Carte XBee :
  - TX : *beeOut*=5
  - RX : *beeIn*=12

Définition supplémentaire : *SEUIL_FACTOR* = 0.5
Cette définition permet de changer le seuil de détection des capteurs. Elle permet de modifié la distance retenue par rapport au calibrage lors de la mise sous tension de la carte Arduino ou après la demande d'un recalibrage de la part d'un utilisateur.

### Librairie arduino utilisée :

```c
#include <SoftwareSerial.h>
```

Cette librairie est installée par défaut.

### A savoir :

Possibilité de débogage de l'application Arduino en la branchant sur un port USB, ou en utilisant les pin 0 *(RX)* et 1 *(TX)*

Possibilité de changer la fréquence d'actualisation des capteurs pour détecter une présence. Pour ce faire il fuat modifier la ligne suivante dans la fonction void loop() : *delay(950);*

```c
// Limite de vitesse de traitement. + affichage de la led arduino pour notifier la nouvelle itération.
delay(950); <-- IL FAUT MODIFIER CELLE CI !
digitalWrite(LED_BUILTIN, HIGH);
delay(50);
digitalWrite(LED_BUILTIN, LOW);
```



## Application Android

### Prérequis

*Android Studio* : [https://developer.android.com/studio](https://developer.android.com/studio)

*SDK*  (min 19): [https://developer.android.com/studio/releases/platforms](https://developer.android.com/studio/releases/platforms)

*Firebase in Android Studio*:

1. [Ajouter Firebase à votre projet Android](https://firebase.google.com/docs/android/setup)

2. Crée un fichier env.xml dans le dossier ress/values

3. Ajouter l'id de la Room correspondant dans la base de donée

```xml
<string name="RoomNumber">ID</string>
```



### Organisation de l'application

  L'application est composé de trois activité android :

1. SplashScreenActivity

2. MainActivity

3. Historique

#### SplashScreenActivity

Cette activité permet de gérer le splashScreen au démarage de l'application



#### MainActivity

La mainActivity est l'activité correspond à l'écran de page d'Acceuil



#### Historique

Cette activité permet l'affichage des données :

1. Entrées

2. Sorties

3. A l'intérieur

## Application "la centrale" :

Faire un fichier .env et y mettre les variables d'ebvironnement suivantes :
- ROOM1
- BAUD
- USB


Installer les packages :
```pip install -r requirements.txt```

Connecter la carte xBee paramétrée en API pour communiquer avec la carte xbee liée à la carte arduino.

Lancer les programme :
```python3 central.py```

