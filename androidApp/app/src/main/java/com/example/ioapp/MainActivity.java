package com.example.ioapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    protected TextView mark;
    protected FirebaseDatabase database;

    // Listener creation for calibration value
    protected ValueEventListener calibrationListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // This method is called once with the initial value and again
            // whenever data at this location is updated.
            String value = dataSnapshot.getValue(String.class);
            // Create a popup if the calibration has realised
            if (value.equals("False")) {
                addPopup();
            }
        }

        @Override
        public void onCancelled(DatabaseError error) {
            // Failed to read value
            Log.e("Failed to read value.", error.toException().toString());
        }
    };

    // Listener creation for rooms value
    protected ValueEventListener myRefCurrentNumberPeopleListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // This method is called once with the initial value and again
            // whenever data at this location is updated.
            Integer value = dataSnapshot.getValue(Integer.class);
            mark.setText(String.valueOf(value));
        }

        @Override
        public void onCancelled(DatabaseError error) {
            // Failed to read value
            Log.e("Failed to read value.", error.toException().toString());
        }
    };

    /*
     *  Function call on Activity creation
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mark = (TextView) findViewById(R.id.mark);

        // get reference from database
        database = FirebaseDatabase.getInstance();
        String myRefCurrentNumberPeoplePath = "Rooms/"+getResources().getString(R.string.RoomNumber) + "/CurrentNumberPeople";
        DatabaseReference myRefCurrentNumberPeople = database.getReference(myRefCurrentNumberPeoplePath);
        DatabaseReference calibration = database.getReference("NeedCalibration");

        // Add Listener on database
        calibration.addValueEventListener(calibrationListener);
        myRefCurrentNumberPeople.addValueEventListener(myRefCurrentNumberPeopleListener);
    }


    /*
     * Function call on Menu creation
     * Set the View
     */
    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        if (menu instanceof MenuBuilder) {
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        MenuItem reload = menu.findItem(R.id.reload_button);
        reload.setIcon(R.drawable.reload_button);
        return true;
    }

    /*
     * Function call on selected Item
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.reload_button:
                sendReloadAlertToFirebase();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * Update value NeedCalibration in the database
     * This function is call on a onClick btn
     */
    public void sendReloadAlertToFirebase() {
        // Write a message to the database
        DatabaseReference myRef = database.getReference("NeedCalibration");
        myRef.setValue("True");
    }

    /*
     * Function associated to button btn_historique
     * Create new Activity Historique
     */
    public void goToHistory(View v) {
        Intent i = new Intent(this, com.example.ioapp.Historique.class);
        startActivity(i);
    }

    /*
     * Create the Popup element element
     */
    public void addPopup() {
        final EditText txtUrl = new EditText(this);
        Toast.makeText(getApplicationContext(), "Calibrage fait", Toast.LENGTH_SHORT).show();
    }

}
