/**
 * NOTE : Attention cette version peut ne pas être fonctionnelle !
 *        Après réflexion et remise en cause du code, la mise en place d'une interface (utilisation de 2 bits au lieu de 3 pour les trois capteurs est faisable, et permettra de sauvegarder les 4 derniers états dans un octet !
 *        Cette version permet de sauvegarder le contenue préfait au cas où divers problèmes surviennent lors de l'implémentation de la nouvelle architecture.
 */
#include <SoftwareSerial.h>

#define lTrigPin 3 //attach pin D3 Arduino to pin Trig of HC-SR04
#define mTrigPin 6
#define rTrigPin 9

#define beeOut 5
#define beeIn 12

#define outLeft 2
#define middle 4
#define outRight 8

#define SEUIL_FACTOR 0.75

int sensorThreshold[3] = {0, 0, 0};
int const trigPinArray[3] = {lTrigPin, mTrigPin, rTrigPin}; // Définition des PIN par capteur.
int const echoPinArray[3] = {outLeft, middle, outRight}; // Définition des PIN par capteur.

int doesNeedCalibration = false; // Dans le cas d'un interrupt pour recalibrer les capteurs

byte sState = 0b0; // Utilisation d'un octet pour stocker 4 états différents

SoftwareSerial xbeeSerial(beeIn, beeOut); //RX, TX

/**
 * Print a bytes in binary (useful for testing)
 */
void printBinary(byte octet) {
  for (int i = 7; i>=0; i--) {
    Serial.print((octet >> i) & 0b1);
  }
  Serial.println();
}

/**
 * 
 * 
 */
int checkSensor(int sensor) {
  long duration;
  int distance;

  // Clears the triggerPin condition
  digitalWrite(trigPinArray[sensor], LOW);
  delayMicroseconds(10);
  digitalWrite(trigPinArray[sensor], HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinArray[sensor], LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPinArray[sensor], HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2;
  delayMicroseconds(10);
  return distance; 
}

// Used to process distance value to return either someone entered or leaved the room.
int processPulse(int distance, int sensor) {
  int resultForDBB = 0;
  byte mask = 0b1 << abs((1-sensor)); //if sensor 0 (left) or 2 (right) the shift is 1. Else (sensor is 1 which is the middle one) then the shift is zero!


  /**
   * Si le seuil ici ne fonctionne pas parce que l'angle de détection est trop grand plusieurs solutions sont faisables.
   * Oeillière
   * Espacement entre cpateurs plus important
   * Sinon réduire le seuil de détection (la distance de détection). On admet prendre en compte les obstacle à moins d'un mètre d ela porte. En faisant cela on réduit l'expansion de détection car elle varie selon la distance.
   * 
   *  ************* AJOUT DU SEUIL_FACTOR : VOIR #DEFINE. Règle le seuil de détection en fonction de la distance du calibrage !
   */
  if (distance <= sensorThreshold[sensor] * SEUIL_FACTOR) {
  //if (distance <= 20) {
    sState = sState | mask;
  }
  // Si on traite le troisième sensor
  if (sensor == 2) {
    // On ne veut pas dupliquer les historiques, donc il faut vérifier que l'état précédent n'est pas le même que celui si
    byte temp = (sState & 0b1100) >> 2;
    if (temp == (sState & 0b11)) {sState = sState & 0xFC;} // Si l'étape précédente est la même qu'actuelle, on réinitialise sState en gardant les 6 bits les plus forts 0b11111100 = 0xFC
    else {
      // pas la même étape, donc vérification des conditions pour détemriner si qqun est rentré ou sortie
      /**
       * Calcul des nombres décimal :
       * 00 -> 10 -> 01 -> 00 = extérieur puis middle puis disparu donc la personne est rentrée
       * 10 -> 11 -> 01 -> 00 = idem
       * 00 -> 01 -> 10 -> 00 = apparition middle puis extérieur donc la personne est sortie
       * 01 -> 11 -> 10 -> 00 = idem
       */
      printBinary(sState);
      if (sState == 0b00100100 || sState == 0b10110100)
        resultForDBB = 1;
      else if (sState == 0b00011000 || sState == 0b01111000)
        resultForDBB = -1;
      // on shift à la fin
      sState = sState << 2;
    }
  }
  return resultForDBB;
}

int calibrate() {
  /**
   * Séquence de calibrage. Cette fonction est exécutée dans setup(). Elle n'est exécuté qu'une seule fois !
   * Mais nous pouvons changer cela pour implémenter un système de recalibrage. Bouton poussoir avec interrupt ?
   */
  int testing = true; // variable de test pour sortir de la boucle.
  int state = 0; // value to be returned
  
  int distArray[3][3] = {0}; // Tableau à deux entrées pour récupérer 3 fois la distance maximum par capteur.
  

  // On fait 3 mesures sur chaque capteur : permet de faire une moyenne des valeurs ensuite pour établir un seuil
  for (int iteration = 0; iteration < 3; iteration++) {
    for (int sensor = 0; sensor < 3; sensor++) {
      // Test de chaque sensor à chaque iteration
      distArray[iteration][sensor] = checkSensor(sensor);
    }
  }

  int temp = 0;
  // Calcul de la moyenne 
  for (int sensor = 0; sensor<3; sensor++) {
    for (int iteration = 0; iteration < 3; iteration++) {
      // Si ce n'est pas la première itération d'un capteur, alors on peut comparer sa valeur à l'itération précédente pour y trouver une cohérence.
      if (iteration != 0) {
        temp = distArray[iteration][sensor];
        //Serial.print("isTempTooFar : ");
        //Serial.println(temp);
        if (temp <= distArray[iteration-1][sensor]*0.90 || temp >= distArray[iteration-1][sensor]*1.10) { // Repère une trop grosse différence entre les valeurs individuelles. Suggère qu'une erreur s'est porduite ! (erreur de 10%)
          Serial.println("An error might occur : value from calibrating are too far from a run to another");
          Serial.print("Ancienne distance : ");
          Serial.println(distArray[iteration-1][sensor]);
          Serial.print("Distance :");
          Serial.println( );
          state = -1; // retourne -1; utile pour savoir si une erreur s'est produite
        }
      }
      sensorThreshold[sensor] += distArray[iteration][sensor]; // Enregistrement des seuils (Addition de la distance à chaque itération.)
    }
    sensorThreshold[sensor] /= 3; // Division de la somme des seuils par la quantité de tests
  }
  // return un code d'erreur;
  // 0 : ok;
  // -1 : problème lors des mesures des valeurs.
  return state;
}

void setup() {
  pinMode(lTrigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(mTrigPin, OUTPUT);
  pinMode(rTrigPin, OUTPUT);
  
  pinMode(outLeft, INPUT); // Sets the echoPin as an INPUT
  pinMode(middle, INPUT);
  pinMode(outRight, INPUT);

  pinMode(beeIn, INPUT);
  pinMode(beeOut, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  // une fois que les pin sont définie : on calibre les capteurs pour récupérer le seuil de détection.
  
  Serial.begin(9600); // // Serial Communication is starting with 9600 of baudrate speed
  xbeeSerial.begin(9600);

  // Calibration après la mise en route des Serials. (permet le debugging)
  calibrate();

  // Affichage des calibrage gauche (l), milieu (m) et droit (r)
  Serial.print("Voici le calibrage (lmr) : ");
  Serial.print(sensorThreshold[0]);
  Serial.print("|");
  Serial.print(sensorThreshold[1]);
  Serial.print("|");
  Serial.println(sensorThreshold[2]);

  digitalWrite(LED_BUILTIN, LOW);
}

  


/**
 * Pour chaque sensor :
 *  calculer sa distance
 *  traiter le calcul de sa distance pour connaitre l'état. 0b10 si l'un des deux capteur extérieur détecte une présence. et 0b01 si le capteur central détecte une présence. sauvegarde sur 2 bits. Donc sauvegarde de 4 états différents sur 1 octet.
 */
void loop() {
  for(int sensor = 0; sensor < 3; sensor++) {
    int distance = checkSensor(sensor);
    Serial.print("Sensor : ");
    Serial.print(sensor);
    Serial.print(" Distance : ");
    Serial.println(distance);
    int result = processPulse(distance, sensor);
    if (result != 0) {
      Serial.println(result);
      xbeeSerial.println(result);
    }
  }
  // Limite de vitesse de traitement. + affichage de la led arduino pour notifier la nouvelle itération.
  delay(450);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);

  // si un caractère est  envoyé sur la xbee, alors on recalibre le module.
  if (xbeeSerial.available() != 0) {
    char rcv = xbeeSerial.read();
    while(xbeeSerial.available() != 0) {xbeeSerial.read();} // tant qu'il y a des caractères à lire, on les lit. Sert pour flush le buffer.
      
    sensorThreshold[0] = 0;
    sensorThreshold[1] = 0;
    sensorThreshold[2] = 0;
    sState = 0b0;
    calibrate();
    xbeeSerial.println("Reset done!"); // On renvoie que le Reset à bien été pris en compte
  }
}
