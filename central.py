#!/usr/bin/env python3
import serial
import time
import os
from digi.xbee.devices import XBeeDevice
from firebase import firebase
from dotenv import load_dotenv
load_dotenv()

BAUDSerial = os.environ.get('BAUD')
device = XBeeDevice(os.environ.get('USB'), BAUDSerial)

# Firebase db link
firebase = firebase.FirebaseApplication(
    'https://iotapp-4c204-default-rtdb.europe-west1.firebasedatabase.app/', None)
firebase.put('/', 'NeedCalibration', "True")

#Open xbee communication
device.open()

#count logic
def my_data_received_callback(xbee_message):
    data = xbee_message.data.decode("utf8")
    if (data != "Reset done!\r\n"):
        print(os.environ.get('ROOM1'))
        count = firebase.get('/Rooms/' + os.environ.get('ROOM1'), 'CurrentNumberPeople')
        print(f"count = {count}")
        print(int(data))
        count = count + int(data)
        firebase.put('/Rooms/' + os.environ.get('ROOM1'),'CurrentNumberPeople',count)
        if int(data) == 1:
            enter = firebase.get('/Rooms/'+ os.environ.get('ROOM1'), 'Enter')
            enter = enter + 1
            firebase.put('/Rooms/'+ os.environ.get('ROOM1'), 'Enter',enter)
        elif int(data) == -1:
            ex= firebase.get('/Rooms/' + os.environ.get('ROOM1'), 'Exit')
            ex = ex + 1
            firebase.put('/Rooms/' + os.environ.get('ROOM1'),'Exit',ex)
        print("data: {}".format(data))

#Main
device.add_data_received_callback(my_data_received_callback)

#calibration logic
while (True):
    try:
        if (firebase.get('/', 'NeedCalibration') == "True"):
            firebase.put('/', 'NeedCalibration', "False")
            device.send_data_broadcast("C")
            print("send character")
        time.sleep(0.001)
    except KeyboardInterrupt:
        break

device.close()
